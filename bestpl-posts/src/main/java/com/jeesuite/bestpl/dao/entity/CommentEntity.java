package com.jeesuite.bestpl.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jeesuite.common.util.DateUtils;
import com.jeesuite.mybatis.core.BaseEntity;

@Table(name = "sc_comment")
public class CommentEntity extends BaseEntity {
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 帖子id
     */
    @Column(name = "post_id")
    private Integer postId;

    /**
     * 语言id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 语言id
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 点赞人数
     */
    @Column(name = "like_count")
    private Integer likeCount;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    private Date createdAt;

    /**
     * 语言名称
     */
    private String content;

    /**
     * 获取主键ID
     *
     * @return id - 主键ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键ID
     *
     * @param id 主键ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取帖子id
     *
     * @return post_id - 帖子id
     */
    public Integer getPostId() {
        return postId;
    }

    /**
     * 设置帖子id
     *
     * @param postId 帖子id
     */
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    /**
     * 获取语言id
     *
     * @return user_id - 语言id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置语言id
     *
     * @param userId 语言id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取语言id
     *
     * @return user_name - 语言id
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置语言id
     *
     * @param userName 语言id
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}

	/**
     * 获取创建时间
     *
     * @return created_at - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取语言名称
     *
     * @return content - 语言名称
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置语言名称
     *
     * @param content 语言名称
     */
    public void setContent(String content) {
        this.content = content;
    }
    
    public String getFormatCreateTime(){
		if(getCreatedAt() == null)return null;
		long diffSeconds = DateUtils.getDiffSeconds(new Date(), getCreatedAt());
		if(diffSeconds >= 2592000){
			return (diffSeconds/2592000) + " 月前";
		}
		if(diffSeconds >= 86400){
			return (diffSeconds/86400) + " 天前";
		}
		if(diffSeconds >= 3600){
			return (diffSeconds/3600) + " 小时前";
		}
		if(diffSeconds >= 60){
			return (diffSeconds/60) + " 分钟前";
		}
		
		return diffSeconds + " 秒前";
		
	}
    
    public String getUserAvatar(){
		return userId % 100 + "/" + userId + ".png";
	}
}