layui.define(['layer','form'], function(exports) {
	"use strict";
    var layer = layui.layer,
    $ = layui.jquery,
    form = layui.form();
    
    form.on('submit(register)',function(data) {
        data = data.field;
        if (data.password != data.repassword) {
            layer.msg('两次密码不一致', {
                icon: 5
            });
            return false;
        }
        var loading = layer.load();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: './act/register',
            contentType: "application/json",
            data: JSON.stringify(data),
            complete: function() {
                layer.close(loading);
            },
            success: function(data) {
            	if(data.errorMsg){
            		layer.msg(data.errorMsg, {icon: 5});
            		return;
            	}
            	setTimeout(function() {
                    window.location.href = '../ucenter/index';
                },
                500);
            },
            error: function(xhr, type) {
                layer.msg('系统错误', {
                    icon: 5
                });
            }
        });
        return false;
    });

    form.on('submit(login)',function(data) {
        var loading = layer.load();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: './act/login',
            contentType: "application/json",
            data: JSON.stringify(data.field),
            complete: function() {
                layer.close(loading);
            },
            success: function(data) {
            	if(data.errorMsg){
            		layer.msg(data.errorMsg, {icon: 5});
            		return;
            	}
            	setTimeout(function() {
                    window.location.href = '../ucenter/index';
                },
                500);
            },
            error: function(xhr, type) {
                layer.msg('系统错误', {
                    icon: 5
                });
            }
        });
        return false;
    });

    exports('user', null);
});