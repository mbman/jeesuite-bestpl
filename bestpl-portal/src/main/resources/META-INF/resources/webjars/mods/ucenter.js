/**

 @Name: 用户模块

 */
 
layui.define(['laypage', 'fly', 'element', 'flow'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  var flow = layui.flow;
  var element = layui.element();

  var gather = {}, dom = {
    mine: $('#LAY_mine')
    ,mineview: $('.mine-view')
    ,minemsg: $('#LAY_minemsg')
    ,infobtn: $('#LAY_btninfo')
  };

  //我的相关数据
  var elemUC = dom.mine, elemUCM = dom.minemsg;
  gather.minelog = {};
  gather.mine = function(index, type, url){
    var tpl = [
      '{{# for(var i = 0; i < d.data.length; i++){ }}\
      <li>\
    	{{# if(d.data[i].isRecommend == 1){ }}\
        <span class="fly-jing">荐</span>\
        {{# } }}\
        <a class="jie-title" href="/post/{{d.data[i].id}}/" target="_blank">{{= d.data[i].title}}</a>\
        <i>{{d.data[i].formatCreateTime}}</i>\
        {{# if(d.data[i].accept == -1){ }}\
        <a class="mine-edit" href="/post/edit/{{d.data[i].id}}">编辑</a>\
        {{# } }}\
        <em>{{d.data[i].commentCount}}评/{{d.data[i].viewCount}}阅</em>\
      </li>\
      {{# } }}'
      ,'{{# for(var i = 0; i < d.data.length; i++){ }}\
      <li>\
      <a class="jie-title" href="/post/{{d.data[i].postId}}/" target="_blank">{{= d.data[i].content}}</a>\
      <i>发表于{{ d.data[i].formatCreateTime }}</i>\
    </li>\
    {{# } }}'
    ];

    var view = function(res){
      var html = laytpl(tpl[type == 'posts' ? 0 : 1]).render(res);
      $('#LAY_count_'+type).html(res.total);
      $('#LAY_list_'+type).html(res.data.length === 0 ? '<div class="fly-msg">没有相关数据</div>' : html);
    };

    var page = function(now){
      var curr = now || 1;
      if(gather.minelog[type + '-page-' + curr]){
        view(gather.minelog[type + '-page-' + curr]);
      } else {
          fly.json(url, {
              page: curr
            }, function(res){
              view(res);
              gather.minelog['mine-jie-page-' + curr] = res;
              now || laypage({
                cont: 'LAY_page_'+type
                ,pages: res.pages
                ,skin: 'fly'
                ,curr: curr
                ,jump: function(e, first){
                  if(!first){
                    page(e.curr);
                  }
                }
              });
            });
          }
    };

    if(!gather.minelog[type]){
      page();
    }
  };

  if(elemUC[0]){
    layui.each(dom.mine.children(), function(index, item){
      var othis = $(item)
      if(othis.data('url'))gather.mine(index, othis.data('type'), othis.data('url'));
    });
  }

  //显示当前tab
  if(location.hash){
    element.tabChange('ucenter', location.hash.replace(/^#/, ''));
  }

  element.on('tab(ucenter)', function(){
    var othis = $(this), layid = othis.attr('lay-id');
    if(layid){
      location.hash = layid;
    }
  });

  //根据ip获取城市
  if($('#LAY_city').val() === ''){
    $.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function(){
      $('#LAY_city').val(remote_ip_info.city);
    });
  }

  //上传图片
  if($('.upload-img')[0]){
    layui.use('upload', function(upload){
      var avatarAdd = $('.avatar-add');
      layui.upload({
        elem: '.upload-img input'
        ,method: 'post'
        ,url: '/ucenter/upload_avatar'
        ,before: function(){
          avatarAdd.find('.loading').show();
        }
        ,success: function(res){
          if(res.status == 0){
            $.post('/ucenter/set/', {
              avatar: res.url
            }, function(res){
              location.reload();
            });
          } else {
            layer.msg(res.msg, {icon: 5});
          }
          avatarAdd.find('.loading').hide();
        }
        ,error: function(){
          avatarAdd.find('.loading').hide();
        }
      });
    });
  }

  //合作平台
  if($('#LAY_coop')[0]){

    //资源上传
    $('#LAY_coop .layui-upload-file').each(function(index, item){
      var othis = $(this);
      layui.upload({
        elem: item
        ,url: '/api/upload/cooperation/?filename='+ othis.data('filename')
        ,type: 'file'
        ,ext: 'zip'
        ,before: function(){
          layer.msg('正在上传', {
            icon: 16
            ,time: -1
            ,shade: 0.7
          });
        }
        ,success: function(res){
          if(res.code == 0){
            layer.msg(res.msg, {icon: 6})
          } else {
            layer.msg(res.msg)
          }
        }
      });
    });

  }

  //提交成功后刷新
  fly.form['set-mine'] = function(data, required){
    layer.msg('修改成功', {
      icon: 1
      ,time: 1000
      ,shade: 0.1
    }, function(){
      location.reload();
    });
  }

  //帐号绑定
  $('.acc-unbind').on('click', function(){
    var othis = $(this), type = othis.attr('type');
    layer.confirm('整的要解绑'+ ({
      qq_id: 'QQ'
      ,weibo_id: '微博'
    })[type] + '吗？', {icon: 5}, function(){
      fly.json('/api/unbind', {
        type: type
      }, function(res){
        if(res.status === 0){
          layer.alert('已成功解绑。', {
            icon: 1
            ,end: function(){
              location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });


  //我的消息
  gather.minemsg = function(){
    var delAll = $('#LAY_delallmsg')
    ,tpl = '{{# var len = d.length;\
    if(len === 0){ }}\
      <div class="fly-none">您暂时没有最新消息</div>\
    {{# } else { }}\
      <ul class="mine-msg">\
      {{# for(var i = 0; i < len; i++){ }}\
        <li data-id="{{d.data[i].id}}">\
          <blockquote class="layui-elem-quote">{{ d.data[i].content}}</blockquote>\
          <p><span>{{d.data[i].time}}</span><a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger fly-delete">删除</a></p>\
        </li>\
      {{# } }}\
      </ul>\
    {{# } }}'
    ,delEnd = function(clear){
      if(clear || dom.minemsg.find('.mine-msg li').length === 0){
        dom.minemsg.html('<div class="fly-none">您暂时没有最新消息</div>');
      }
    }
    
    
    fly.json('/message/find/', {}, function(res){
      var html = laytpl(tpl).render(res);
      dom.minemsg.html(html);
      if(res.data.length > 0){
        delAll.removeClass('layui-hide');
      }
    });
    
    //阅读后删除
    dom.minemsg.on('click', '.mine-msg li .fly-delete', function(){
      var othis = $(this).parents('li'), id = othis.data('id');
      fly.json('/message/remove/', {
        id: id
      }, function(res){
        if(res.status === 0){
          othis.remove();
          delEnd();
        }
      });
    });

    //删除全部
    $('#LAY_delallmsg').on('click', function(){
      var othis = $(this);
      layer.confirm('确定清空吗？', function(index){
        fly.json('/message/remove/', {
          all: true
        }, function(res){
          if(res.status === 0){
            layer.close(index);
            othis.addClass('layui-hide');
            delEnd(true);
          }
        });
      });
    });

  };

  dom.minemsg[0] && gather.minemsg();

  exports('ucenter', null);
  
});