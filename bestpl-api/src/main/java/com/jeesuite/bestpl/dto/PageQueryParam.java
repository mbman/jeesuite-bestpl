/**
 * 
 */
package com.jeesuite.bestpl.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2017年3月24日
 */
public class PageQueryParam  implements Serializable{

	private static final long serialVersionUID = 1L;

	private int pageNo = 1;
	private int pageSize = 15;
	private String orderBy;
	private Map<String, Object> conditions;
	
	public PageQueryParam() {}
	
	
	
	public PageQueryParam(int pageNo) {
		super();
		this.pageNo = pageNo;
	}

    

	public PageQueryParam(int pageNo, int pageSize) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}



	public PageQueryParam(int pageNo, int pageSize, String orderBy, Map<String, Object> conditions) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.orderBy = orderBy;
		this.conditions = conditions;
	}

	public PageQueryParam(int pageNo, int pageSize, Map<String, Object> conditions) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.conditions = conditions;
	}

	public int getPageNo() {
		return pageNo <= 0 ? 1 : pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public Map<String, Object> getConditions() {
		return conditions == null ? (conditions = new HashMap<>()) : conditions;
	}

	public void setConditions(Map<String, Object> conditions) {
		this.conditions = conditions;
	}

	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	
}
